def first_problem():
    a = 1
    s = 0

    while a < 500:
        s = s+a
        a = a +2 

    return a, s

print(first_problem())

# output -> descnto, valor_a_pagar

def second_problem(valor_da_compra):
    if valor_da_compra >= 250:
        return 'O desconto é 20%', valor_da_compra - (valor_da_compra * 0.20)
    else:
        return "O desconto é 15%", valor_da_compra - (valor_da_compra * 0.15)


print(second_problem(250))

print(second_problem(249))

print(second_problem(251))



def third_problem(celcius):
    f = (9 * celcius + 160 )/ 5 

    return f


print(third_problem(40))



def fourth_problem(r ,h):
    
    cubic_meters = round(1.97 ** 3, 2)

    area = 3.14 * ((r**2)*h)

    return round(cubic_meters * area), area


print(fourth_problem(3, 5.5))


# distancia entre dois pontos
#  a(x1,y1) e b(x2,y2)

from math import sqrt

x = (5, 20)
y = (10, 25)


def fifth_problem(x, y):
    distAB = sqrt((x[0]-y[0])**2) + ((x[1]-y[1])**2)
    
    return distAB

def fifth_problem_2 (x, y):
    distAB = ((x[0]-y[0])**2) + ((x[1]-y[1])**2) ** 0.5

    return distAB


print(fifth_problem(x,y), fifth_problem_2(x,y))

# Link correto do notion: https://ambiguous-swan-06f.notion.site/1-O-que-Python-7f5b5a8642ed44bc82dbb6e13ae1ff21
# meu linkedin: https://www.linkedin.com/in/gabrielfabianoo/
# meu insta: @gabrielfabianoo

def sixth_problem(n):
    ultimo = 1
    penultimo = 1
    termo = 0

    if(n==1 or n==2):
        termo = 1
        return termo
    else: 
        for count in range(2,n):
            termo = ultimo + penultimo 
            penultimo = ultimo
            ultimo = termo
            count += 1
        return termo

print(sixth_problem(102))

